defmodule Tasker.Commons do

  def mapErrors([], acc), do: acc
  def mapErrors([h|t], acc) do
     mapErrors(t,Map.put(acc, elem(h, 0), elem(h,1) |> elem(0)))
  end

end