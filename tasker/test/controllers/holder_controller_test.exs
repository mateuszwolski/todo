defmodule Tasker.HolderControllerTest do
  use Tasker.ConnCase

  alias Tasker.Holder
  @valid_attrs %{canceled: 42, done: 42, total: 42}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, holder_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing counter"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, holder_path(conn, :new)
    assert html_response(conn, 200) =~ "New holder"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, holder_path(conn, :create), holder: @valid_attrs
    assert redirected_to(conn) == holder_path(conn, :index)
    assert Repo.get_by(Holder, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, holder_path(conn, :create), holder: @invalid_attrs
    assert html_response(conn, 200) =~ "New holder"
  end

  test "shows chosen resource", %{conn: conn} do
    holder = Repo.insert! %Holder{}
    conn = get conn, holder_path(conn, :show, holder)
    assert html_response(conn, 200) =~ "Show holder"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, holder_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    holder = Repo.insert! %Holder{}
    conn = get conn, holder_path(conn, :edit, holder)
    assert html_response(conn, 200) =~ "Edit holder"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    holder = Repo.insert! %Holder{}
    conn = put conn, holder_path(conn, :update, holder), holder: @valid_attrs
    assert redirected_to(conn) == holder_path(conn, :show, holder)
    assert Repo.get_by(Holder, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    holder = Repo.insert! %Holder{}
    conn = put conn, holder_path(conn, :update, holder), holder: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit holder"
  end

  test "deletes chosen resource", %{conn: conn} do
    holder = Repo.insert! %Holder{}
    conn = delete conn, holder_path(conn, :delete, holder)
    assert redirected_to(conn) == holder_path(conn, :index)
    refute Repo.get(Holder, holder.id)
  end
end
