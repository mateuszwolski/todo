defmodule Tasker.HolderTest do
  use Tasker.ModelCase

  alias Tasker.Holder

  @valid_attrs %{canceled: 42, done: 42, total: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Holder.changeset(%Holder{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Holder.changeset(%Holder{}, @invalid_attrs)
    refute changeset.valid?
  end
end
