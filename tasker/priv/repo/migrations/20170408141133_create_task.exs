defmodule Tasker.Repo.Migrations.CreateTask do
  use Ecto.Migration

  def change do
    create table(:tasks) do
      add :description, :string
      add :category_id, references(:categories, on_delete: :nothing)

      timestamps()
    end
    create index(:tasks, [:category_id])

  end
end
