defmodule Tasker.Repo.Migrations.CreateHolder do
  use Ecto.Migration

  def change do
    create table(:counter) do
      add :done, :integer
      add :canceled, :integer
      add :total, :integer

      timestamps()
    end

  end
end
