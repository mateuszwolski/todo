defmodule Tasker.Api.StatisticsView do
  use Tasker.Web, :view

  def render("stat.json", %{stat: stat}) do
      %{
        total: stat.total,
        done: stat.done,
        canceled: stat.canceled
      }
    end
  end
