defmodule Tasker.Api.TaskView do
  use Tasker.Web, :view

    def render("error.json", %{changeset: changeset}) do
    %{err: changeset}
    end

   def render("ok.json", _) do
     %{}
   end

   def render("action.json", %{action: action}) do
     %{"action": action}
   end

  def render("show.json", %{task: task}) do
    %{
      task: render_one(task, __MODULE__, "details.json"),
    }
  end

   def render("index.json", %{tasks: tasks}) do
     %{
       tasks: render_many(tasks, __MODULE__, "task.json"),
     }

   end

  def render("task.json", %{task: task}) do
      %{
        _links: %{
          self: %{
            href: "/api/tasks/#{task.id}/",
          },
          category: %{
            href: "/api/categories/#{task.category_id}/",
         }
        },
        description: task.description,
        category_id: task.category_id
      }
    end


  def render("details.json", %{task: task}) do
      %{
        _links: %{
          self: %{
            href: "/api/tasks/#{task.id}/",
          },
          category: %{
            href: "/api/categories/#{task.category_id}/",
         }
        },
        description: task.description,
        category: task.category
      }
    end

  end