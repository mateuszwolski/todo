defmodule Tasker.Api.CategoryView do
  use Tasker.Web, :view

    def render("error.json", %{changeset: changeset}) do
    %{err: changeset}
    end

   def render("ok.json", _) do
     %{}
   end

  def render("show.json", %{category: category}) do
    %{
      category: render_one(category, __MODULE__, "category.json"),
    }
  end

   def render("index.json", %{categories: categories}) do
     %{
       categories: render_many(categories, __MODULE__, "category.json"),
     }

   end

  def render("category.json", %{category: category}) do
      %{
        _links: %{
          self: %{
            href: "/api/categories/#{category.id}/",
          },
        },
        id: category.id,
        name: category.name
      }
    end
  end
