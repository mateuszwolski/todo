defmodule Tasker.Category do
  use Tasker.Web, :model

  schema "categories" do
    field :name, :string
    has_many :task, Tasker.Task, on_delete: :delete_all

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
