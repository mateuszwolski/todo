defmodule Tasker.Holder do
  use Tasker.Web, :model

  schema "counter" do
    field :done, :integer
    field :canceled, :integer
    field :total, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:id, :done, :canceled, :total])
    |> validate_required([:id, :done, :canceled, :total])
  end
end
