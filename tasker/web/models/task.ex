defmodule Tasker.Task do
  use Tasker.Web, :model

  schema "tasks" do
    field :description, :string
    belongs_to :category, Tasker.Category

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
  IO.inspect params

    struct
    |> cast(params, [:description, :category_id])
    |> validate_required([:description, :category_id])
  end
end
