defmodule Tasker.Router do
  use Tasker.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api/tasks", Tasker.Api do
      pipe_through :api

      get "/", TaskController, :index
      get "/:id", TaskController, :show
      post "/", TaskController, :create
      put "/:id", TaskController, :update
      delete "/:id", TaskController, :delete
      delete "/:id/done", TaskController, :done
      delete "/:id/cancel", TaskController, :cancel


  end

  scope "/api/categories", Tasker.Api do
      pipe_through :api

      get "/", CategoryController, :index
      get "/:id", CategoryController, :show
      post "/", CategoryController, :create
      put "/:id", CategoryController, :update
      delete "/:id", CategoryController, :delete

   end

  scope "/api/stat", Tasker.Api do
      pipe_through :api

      get "/", StatisticsController, :index
  end

  scope "/", Tasker do
    pipe_through :browser # Use the default browser stack
    resources "/categories", CategoryController
    resources "/", TaskController
    delete "/cancel/:id", TaskController, :cancel

  end

  # Other scopes may use custom stacks.
  # scope "/api", Tasker do
  #   pipe_through :api
  # end
end