defmodule Tasker.Api.CategoryController do
   use Tasker.Web, :controller
   alias Tasker.Category
   alias Tasker.Commons

   plug :scrub_params, "category" when action in [:create, :update]


   def index(conn, _params) do
       categories = Repo.all(Category)
       render(conn, "index.json", categories: categories)
   end

   def show(conn, %{"id" => id}) do
        category = Repo.get!(Category, id)
        render(conn, "show.json", category: category)
   end

   def create(conn, %{"category" => category_params}) do
         changeset = Category.changeset(%Category{}, category_params)

         case Repo.insert(changeset) do
           {:ok, category} ->
              conn
              |> put_status(:created)
              |> put_resp_header("location", category_path(conn, :show, category))
              |> render("show.json", category: category)
           {:error, changeset} ->
              conn
              |> put_status(:unprocessable_entity)
              |> render("error.json", changeset: Commons.mapErrors(changeset.errors, %{}))
         end
   end

   def update(conn, %{"id" => id, "category" => category_params}) do
         category = Repo.get!(Category, id)
         changeset = Category.changeset(category, category_params)

         case Repo.update(changeset) do
            {:ok, category} ->
                category = Repo.get!(Category, id)
                render(conn, "show.json", category: category)
            {:error, changeset} ->
                conn
                |> put_status(:unprocessable_entity)
                |> render("error.json", changeset: Commons.mapErrors(changeset.errors, %{}))
         end
   end

   def delete(conn, %{"id" => id}) do
         category = Repo.get!(Category, id)
         Repo.delete!(category)
         render(conn, "ok.json")
   end

end