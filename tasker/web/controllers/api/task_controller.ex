defmodule Tasker.Api.TaskController do
  use Tasker.Web, :controller
  alias Tasker.Task
  alias Tasker.Category
  alias Tasker.Holder
  alias Tasker.Commons

  plug :scrub_params, "task" when action in [:create, :update]

 @action %{delete: "deleted",
            done: "done",
            cancel: "canceled"}

  def index(conn, _params) do
        tasks = Repo.all(Task)
        render(conn, "index.json", tasks: tasks)
  end

   def show(conn, %{"id" => id}) do
        task = Repo.get!(Task, id)
        categories = getCategories(valKey)
        render(conn, "show.json", task: getCustomTask(task, categories))
   end

    def create(conn, %{"task" => task_params}) do
      catId = getCategories(keyVal)[task_params["category"]]
      changeset = Task.changeset(%Task{}, %{description: task_params["description"], category_id: catId})

         case Repo.insert(changeset) do
           {:ok, task} ->
              updateStat(fn h -> %{total: h.total + 1} end)
              conn
              |> put_status(:created)
              |> put_resp_header("location", category_path(conn, :show, task))
              |> render("show.json", task: getCustomTask(task, getCategories(valKey)))
           {:error, changeset} ->
              conn
              |> put_status(:unprocessable_entity)
              |> render("error.json", changeset: Commons.mapErrors(changeset.errors, %{}))
         end
     end

    def update(conn, %{"id" => id, "task" => task_params}) do
           task = Repo.get!(Task, id)
           changeset = Task.changeset(task, task_params)

          case Repo.update(changeset) do
            {:ok, task} ->
              task = Repo.get!(Task, id)
              render(conn, "show.json", task: getCustomTask(task, getCategories(valKey)))
            {:error, changeset} ->
              conn
              |> put_status(:unprocessable_entity)
              |> render("error.json", changeset: Commons.mapErrors(changeset.errors, %{}))
          end
    end

    def delete(conn, %{"id" => id}) do
         task = Repo.get!(Task, id)
         globalDelete(conn, task, @action.delete)
    end


    def done(conn, %{"id" => id}) do
         task = Repo.get!(Task, id)
         updateStat(fn h -> %{done: h.done + 1} end)
         globalDelete(conn, task, @action.done)
    end

    def cancel(conn, %{"id" => id}) do
         task = Repo.get!(Task, id)
         updateStat(fn h -> %{canceled: h.canceled + 1} end)
         globalDelete(conn, task, @action.cancel)
    end

    defp valKey, do: fn acc, key, val -> Map.put(acc, val, key) end
    defp keyVal, do: fn acc, key, val -> Map.put(acc, key, val) end

    defp globalDelete(conn, task, action) do
      case Repo.delete(task) do
                  {:ok, task} ->
                    render(conn, "action.json", action: action)
                  {:error, changeset} ->
                    conn
                    |> put_status(:unprocessable_entity)
                    |> render("error.json", changeset: Commons.mapErrors(changeset.errors, %{}))
                end
    end

    defp getCategories(meth) do
        from(c in Category, select: {c.name, c.id})
        |> Repo.all
        |> Enum.reduce(%{}, fn({key, val}, acc) -> meth.(acc, key, val) end);
    end

    defp updateStat(meth) do
      holder =
            case Repo.get(Holder, 1) do
              nil -> %Holder{id: 1, done: 0, canceled: 0, total: 0}
              hol -> hol
            end

       Holder.changeset(holder, meth.(holder))
       |> Repo.insert_or_update

    end

    defp getCustomTask(task, categories) do
        %{id: task.id,
        description: task.description,
        category: categories[task.category_id],
        category_id: task.category_id}
    end

end