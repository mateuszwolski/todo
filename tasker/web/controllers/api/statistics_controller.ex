defmodule Tasker.Api.StatisticsController do
   use Tasker.Web, :controller
   alias Tasker.Holder

     def index(conn, _params) do
       render(conn, "stat.json", stat: Repo.get!(Holder, 1))
     end
end