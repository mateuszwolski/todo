defmodule Tasker.TaskController do
  use Tasker.Web, :controller

  alias Tasker.Task
  alias Tasker.Category
  alias Tasker.Holder



  def index(conn, _params) do
    tasks = Repo.all(Task)
    counter = Repo.all(Holder)
    categories = Enum.reduce(getCategories(), %{}, fn({key, val}, acc)-> Map.put(acc, val, key) end);
    render(conn, "index.html", tasks: tasks, categories: categories, counter: counter)
  end

  def new(conn, _params) do
    changeset = Task.changeset(%Task{})
    render(conn, "new.html", changeset: changeset, categories: getCategories())
  end

  def create(conn, %{"task_controller.ex" => task_params}) do
    changeset = Task.changeset(%Task{}, task_params)

    case Repo.insert(changeset) do
      {:ok, _task} ->
        funk(fn h -> %{total: h.total + 1} end)
        conn
        |> put_flash(:info, "Task created successfully.")
        |> redirect(to: task_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, categories: getCategories())
    end
  end

  def show(conn, %{"id" => id}) do
    task = Repo.get!(Task, id)
    categories = Enum.reduce(getCategories(), %{}, fn({key, val}, acc)-> Map.put(acc, val, key) end);
    render(conn, "show.html", task: task, categories: categories)
  end

  def edit(conn, %{"id" => id}) do
    task = Repo.get!(Task, id)
    changeset = Task.changeset(task)
    render(conn, "edit.html", task: task, changeset: changeset, categories: getCategories())
  end

  def update(conn, %{"id" => id, "task_controller.ex" => task_params}) do
    task = Repo.get!(Task, id)
    changeset = Task.changeset(task, task_params)

    case Repo.update(changeset) do
      {:ok, task} ->
        conn
        |> put_flash(:info, "Task updated successfully.")
        |> redirect(to: task_path(conn, :show, task))
      {:error, changeset} ->
        render(conn, "edit.html", task: task, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    task = Repo.get!(Task, id)
    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(task)

    funk(fn h -> %{done: h.done + 1} end)

    conn
    |> put_flash(:info, "Task deleted successfully.")
    |> redirect(to: task_path(conn, :index))
  end

  def cancel(conn, %{"id" => id}) do
    task = Repo.get!(Task, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(task)

    funk(fn h -> %{canceled: h.canceled + 1} end)

    conn
    |> put_flash(:info, "Task canceled :(.")
    |> redirect(to: task_path(conn, :index))
  end

  defp getCategories() do
      from(c in Category, select: {c.name, c.id})
      |> Repo.all
  end

   defp funk(meth) do
     holder =
           case Repo.get(Holder, 1) do
             nil  -> %Holder{id: 1, done: 0, canceled: 0, total: 0}
             hol -> hol
            end
     Holder.changeset(holder, meth.(holder))
     |> Repo.insert_or_update
    end
end
